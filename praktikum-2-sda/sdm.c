#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct list
{
    int data;
    struct list* next;
    struct list* prev;
};

struct dbl
{
    struct list* head;
    struct list* tail;
    int size;
};

void push(struct dbl* stacc, int val)
{
    struct list* newn = (struct list*)malloc(sizeof(struct list));
    newn->data = val;
    newn->next = NULL;
    newn->prev = NULL;
    if(stacc->head==NULL)
    {
        stacc->head = newn;
        stacc->tail = newn;
        // (*stacc)->size = 1;
        return;
    }

    stacc->head->prev = newn;
    newn->next = stacc->head;
    stacc->head = newn;
    // (*stacc)->size++;
    return;
}

void popf(struct dbl* stack)
{
    int ret;
    struct list* temp = (struct list*)malloc(sizeof(struct list));
    temp = stack->head;
    // (*stack)->size--;
    if(stack->head==NULL) return;

    // printf("masukpopnya\n");
    stack->head = stack->head->next;
    // printf("test1\n");
    if(stack->head!=NULL)stack->head->prev = NULL;
    // printf("test2\n");
    // ret = temp->data;
    free(temp);
    return;
}

void popb(struct dbl* stack)
{
    int ret;
    struct list* temp = (struct list*)malloc(sizeof(struct list));
    temp = stack->tail;
    // (*stack)->size--;
    if(stack->tail==NULL) return;

    stack->tail = stack->tail->prev;
    if(stack->tail!=NULL) stack->tail->next = NULL;
    // ret = temp->data;
    free(temp);
    return;
}

void popm(struct dbl* stack, int val)
{
    if(stack->head==NULL || stack->tail == NULL) return;

    struct list* temp = (struct list*)malloc(sizeof(struct list));
    struct list* temp1 = (struct list*)malloc(sizeof(struct list));
    temp = stack->head;
    while(temp->next != NULL && temp->next->data != val)
    {
        temp = temp->next;
    }

    temp1 = temp->next;
    if(temp->next == NULL) 
    {
        popb(stack);
        return;
    }
    temp->next = temp->next->next;
    free(temp1);
    return;
}

void print(struct dbl* stack)
{
    struct list* temp = (struct list*)malloc(sizeof(struct list));
    temp = stack->head;
    // if(temp == NULL) printf("kosong\n");
    while(temp!=NULL)
    {
        printf("%d ", temp->data);
        temp = temp->next;
    }
}
// int top(struct stack* stacc)
// {
//     return stacc->data;
// }

int main()
{
    int temp[102];
    memset(temp,0,sizeof(temp));
    int tc, cmd, val, st = 0, qu = 0, erroe = 0, e = 0;
    scanf("%d", &tc);
    // printf("a\n");
    struct dbl* ll = (struct dbl*)malloc(sizeof(struct dbl));
    // printf("a\n");    
    ll->head = NULL;
    ll->tail = NULL;
    ll->size = 0;

    while(tc--)
    {
        // if(ll->head==NULL || ll->tail == NULL) printf("skrg kosong\n");
        // else
        // {
        //     printf("isi: ");
        //     print(ll);
        // // if(ll->head!=NULL)    printf("head: %d tail: %d\n", ll->head->data, ll->tail->data);
        //     printf("\n");
        // }
        
    // printf("st: %d qu: %d e: %d err: %d\n", st, qu, e, erroe);

        scanf("%d %d", &cmd, &val);
        // printf("cmd: %d val: %d\n", cmd, val);
        if(cmd==1)
        {
            // printf("mau push\n");
            push(ll, val);
            temp[val]++;
            ll->size++;
            // printf("done push\n");

        }
        else if(cmd==2)
        {
            if(ll->size == 0)
            {
                // printf("test1\n");
                e = 1; 
                // erroe = 1; 
            }
            else if(ll->head==ll->tail && val == ll->head->data)
            {
                // printf("test2\n");
                if(!(st) && !(qu))
                {
                    st = 1;
                    qu = 1;
                }
                
                popf(ll);
                temp[val]--;
            }
            else if(val==ll->head->data && val == ll->tail->data)
            {
                // printf("test7\n");
                if(st == qu)
                {
                    st = 1;
                    qu = 1;
                }
                popb(ll);
                temp[val]--;
            }
            else if(val==ll->head->data)
            {
                // printf("test3\n");
                // st = 1;
            // printf("mau ppop1\n");                
                popf(ll);
                temp[val]--;
                if(st) 
                {
                    st = 1;
                    if(qu) qu = 0;
                }
                if(!(erroe) && !(qu)) st = 1;
            // printf("doen ppop\n");                
            }
            else if(val==ll->tail->data)
            {
                // printf("test4\n");
                // qu = 1;
                popb(ll);
                temp[val]--;
                if(qu && st) 
                {
                    // qu = 1;
            // printf("mau ppop2\n");                
                    st = 0;
                }
                if(!(erroe) && !(st)) qu = 1;
            // printf("done ppop\n");                
            }
            else if(!(temp[val])) 
            {
                // printf("test5\n");
                st = 0;
                qu = 0;
                erroe = 1;
            }
            else
            {
                // printf("test6\n");
                popm(ll, val);
                temp[val]--;
                st = 0;
                qu = 0;
                erroe = 1;
            }
            ll->size--;
        }
    }

    // printf("st: %d qu: %d e: %d err: %d\n", st, qu, e, erroe);
    if(e) printf("error nih :(\n");
    else if(erroe) printf("tidak tahu\n");
    else if(qu && st) printf("queue stack\n");
    else if(st) printf("stack\n");
    else if(qu) printf("queue\n");
    else printf("tidak tahu\n");

    // printf("\n");
}