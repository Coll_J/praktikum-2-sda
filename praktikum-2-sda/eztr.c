#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct stack
{
    char data;
    int size;
    struct stack* next;
};

void push(struct stack** stacc, char val)
{
    struct stack* newn = (struct stack*)malloc(sizeof(struct stack));
    newn->data = val;
    newn->next = NULL;
    if(*stacc==NULL)
    {
        *stacc = newn;
        (*stacc)->size = 1;
        return;
    }

    newn->next = *stacc;
    *stacc = newn;
    (*stacc)->size++;
    return;
}

char pop(struct stack** stack)
{
    char ret;
    struct stack* temp = (struct stack*)malloc(sizeof(struct stack));
    temp = *stack;
    (*stack)->size--;
    *stack = (*stack)->next;
    ret = temp->data;
    free(temp);
    return ret;
}

char top(struct stack* stacc)
{
    return stacc->data;
}

int main()
{
    int i;
    char input[100002];
    scanf("%s", input);
    struct stack* stacc = (struct stack*)malloc(sizeof(struct stack));
    int len = strlen(input);
    for(i=0;i<len;i++)
    {
        if(input[i]=='(' || input[i]=='{' || input[i]=='[')
        {
            push(&stacc, input[i]);
        }
        else if(input[i]==')' || input[i]=='}' || input[i]==']')
        {
            if(input[i]==')' && top(stacc)=='(') pop(&stacc);
            else if(input[i]=='}' && top(stacc)=='{') pop(&stacc);
            else if(input[i]==']' && top(stacc)=='[') pop(&stacc);
        }
    }

    if(!(stacc->size)) printf(":v\n");
    else printf("v:\n");
}