#include <stdio.h>
#include <stdlib.h>

struct stack
{
    int data;
    // int size;
    struct stack* next;
    struct stack* prev;
};

struct st
{
    struct stack* head;
    struct st* next;
    struct st* prev;
};

void push(struct st* stk, int val)
{
    // printf("masuk push\n");
    struct stack* newn = (struct stack*)malloc(sizeof(struct stack));
    newn->data = val;
    newn->next = NULL;
    newn->prev = NULL;

    // printf("tws1\n");
    if(stk->head == NULL)
    {
        // printf("tws2\n");
        stk->head = newn;
        // printf("tws3\n");
        return;
    }
    
    // printf("tws4\n");
    stk->head->prev = newn;
    // printf("tws5\n");
    newn->next = stk->head;
    // printf("tws6\n");
    stk->head = newn;
    // printf("tws7\n");
    return;
}

int findPath(struct st* stk, struct stack* stkd, int x) 
{
    // printf("masuk rekursi\n");
    stkd->data = 2;
    int i;
    if (stk->next == NULL && stkd->next == NULL) {
        // free(temp);
        return 1;
    }

    struct stack* temp1 = NULL;
    struct stack* temp2 = NULL;
    if(stk->next != NULL)
    {
        temp1 = stk->next->head;
        for(i=0; i<x; i++)
        {
            temp1 = temp1->next;
        }
    }
    if(stk->prev != NULL)
    {
        temp2 = stk->prev->head;
        for(i=0; i<x; i++)
        {
            temp2 = temp2->next;
        }
    }
    
    if (stkd->next != NULL && stkd->next->data == 1) {
        if (findPath(stk, stkd->next, x + 1)) {
            // free(temp);
            return 1;
        }
    }
    if (stkd->prev!=NULL && stkd->prev->data == 1) {
        if (findPath(stk, stkd->prev, x - 1)) {
            // free(temp);
            return 1;
        }
    }
    if (stk->next!=NULL && temp1->data == 1) {
        if (findPath(stk->next, temp1, x)) {
            // free(temp);
            return 1;
        }
    }
    if (stk->prev !=NULL && temp2->data == 1) {
        if (findPath(stk->prev, temp2, x)) {
            // free(temp);
            return 1;
        }
    }


    return 0;
}

int main()
{
    int i,n,t, a;
    scanf("%d", &n);
    struct st* head = (struct st*)malloc(sizeof(struct st));
    head->head = NULL;
    head->next = NULL;
    head->prev = NULL;

    for(i=1;i<n;i++)
    {
        struct st* newh = (struct st*)malloc(sizeof(struct st));
        head->prev = newh;
        newh->next = head;
        head = newh;
    }
    // struct stack* st = (struct stack*)malloc(sizeof stack);
    // for(i=0;i<n;i++)
    // {
    //     (*stacc)[i] = (struct stack)malloc(sizeof(struct stack));
        // stacc[i] = NULL;
    // }
    struct st* temp = (struct st*)malloc(sizeof(struct st));
    struct stack* temp1 = (struct stack*)malloc(sizeof(struct stack));

    temp = head;
    for(i=0;i<n;i++)
    {
        // printf("baris: %d\n", i+1);
        for(t=0;t<n;t++)
        {
            scanf("%d", &a);
            push(temp, a);
        }

        temp = temp->next;
    }

    // printf("\n");
    // temp = head;
    // for(i=0;i<n;i++)
    // {
    //     temp1 = temp->head;
    //     for(t=0;t<n;t++)
    //     {
    //         printf("%d ", temp1->data);
    //         temp1 = temp1->next;
    //         // push(temp, a);
    //     }
    //     printf("\n");
    //     temp = temp->next;
    // }

    if(findPath(head, head->head, 0)) printf("Ada jalan yaa Thanus\n");
    else printf("Buntu yaa Thanus\n");
}