#include <stdio.h>
#include <stdlib.h>

struct deque
{
    int data;
    int hari;
    struct deque* next;
    struct deque* prev;
};

struct deq
{
    struct deque* head;
    struct deque* tail;
    int size;
};

void pushf(struct deq* deck, int val, int hr)
{
    struct deque* newn = (struct deque*)malloc(sizeof(struct deque));
    newn->data = val; 
    newn->hari = hr;
    newn->next = NULL;
    newn->prev = NULL;

    if(deck->head==NULL)
    {
        deck->head = newn;
        deck->tail = newn;
        deck->size++;
        return;
    }

    deck->head->prev = newn;
    newn->next = deck->head;
    deck->head = newn;
    deck->size++;
    return;
}

void pushb(struct deq* deck, int val, int hr)
{
    struct deque* newn = (struct deque*)malloc(sizeof(struct deque));
    newn->data = val; 
    newn->hari = hr;
    newn->next = NULL;
    newn->prev = NULL;

    if(deck->head==NULL)
    {
        deck->head = newn;
        deck->tail = newn;
        deck->size++;
        return;
    }

    deck->tail->next = newn;
    newn->prev = deck->tail;
    deck->tail = newn;
    deck->size++;
    return;
}

int popf(struct deq* deck)
{
    if(deck->head == NULL || deck->tail == NULL) return 0;
    struct deque* temp = (struct deque*)malloc(sizeof(struct deque));
    temp = deck->head;
    int t = temp->data;
    deck->head = deck->head->next;
    if(deck->head != NULL) deck->head->prev = NULL;
    deck->size--;
    free(temp);
    return t;
}

int popb(struct deq* deck)
{
    if(deck->head == NULL || deck->tail == NULL) return 0;
    struct deque* temp = (struct deque*)malloc(sizeof(struct deque));
    temp = deck->tail;
    int t = temp->data;
    deck->tail = deck->tail->prev;
    if(deck->tail != NULL) deck->tail->next = NULL;
    deck->size--;
    free(temp);
    return t;
}

int top(struct deq* deck)
{
    return deck->head->data;
}

int end(struct deq* deck)
{
    return deck->tail->data;
}

void printb(struct deq* deck)
{
    struct deque* temp = (struct deque*)malloc(sizeof(struct deque));
    temp = deck->tail;
    while(temp!=NULL)
    {
        printf("%d ", temp->data);
        temp = temp->prev;
    }
}

int main()
{
  int s;
    int p,n,k,a,nk,c,hr,h,q,maks,cmd;
    struct deq* q1 = (struct deq*)malloc(sizeof(struct deq*));
    struct deq* q2 = (struct deq*)malloc(sizeof(struct deq*));

    q1->head = NULL;
    q1->tail = NULL;
    q1->size = 0;

    q2->head = NULL;
    q2->tail = NULL;
    q2->size = 0;

    // printf("test\n");
    scanf("%d %d", &n, &k);
    // printf("%d %d\n", n, k);
    nk = n-k+1;
    c = 1;
    hr = 1;
    while(n--)
    {
        scanf("%d", &a);
        while(q1->head!=NULL && q1->head->data<a)
        {
            popf(q1);
        }
        pushf(q1, a, 0);
        if(c>=k && q2->size<nk)
        {
            if(q1->size<k)
            {
                p = end(q1);
                pushf(q2, p, hr);
                hr++;
            }
            else
            {
                p = popb(q1);
                pushf(q2, p, hr);
                hr++;
            }
        }
        c++;
    }

    q1->head = NULL;
    q1->tail = NULL;
    q1->size = 0;
    struct deq* q3 = (struct deq*)malloc(sizeof(struct deq*));
    q3->head = NULL;
    q3->tail = NULL;
    q3->size = 0;

    hr = 0;
    scanf("%d %d", &h, &q);
    while(h!=-1 || q!= -1)
    {
        if(q1->head!=NULL || q1->size!=0) maks = top(q1);
        else maks = 0;
        nk = h-hr;
        hr = h;
        while(nk--)
        {
            p = popb(q2);
            pushf(q3, p, 0);
            if(p>=maks) 
            {
                pushf(q1, p, 0);
                maks = p;
            }
        }
        while(q--)
        {
            scanf("%d", &cmd);
            switch(cmd)
            {
                case 1:
                if(q3==NULL || q3->size == 0) printf("Stok abis\n");
                else
                {
                    p = popf(q3);
                    if(p == top(q1)) popf(q1);
                    printf("Nyam\n");
                }
                break;
                case 2:
                if(q3==NULL || q3->size == 0) printf("Stok abis\n");
                else
                {
                    printf("%d\n", top(q1));
                }
                break;
            }
        }
        scanf("%d %d", &h, &q);
    }
}
