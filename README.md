# SOAL
<!-- === -->

1. [AHHA](#ahha)
2. [EZTR](#eztr)
3. [SDM](#sdm)
4. [TDL](#tdl)

## AHHA
<!-- --- -->
Soal
---
Disuruh bantu bobby buat tentuin indomie yang diambil hari itu dengan syarat ambil stok tertinggi dari stok yang bisa diambil hari itu(k).

contoh:

yang di stok 1 2 3 4 5

pengambilan(k): 3

hari pertama, yang bisa diambil: **1 2 3** 4 5

hari kedua, yang bisa diambil: 1 **2 3 4** 5

dan seterusnya.

Terus bantu bobby dengan command:

**1** makan indomie, kalo ada stoknya print "Nyam" kalo abis "Stok abis"

**2** pamer, ambil angka tertinggi dari stok yang ada saat itu, kalau tidak ada, print "Stok abis"

Jawab
---
### file code [disini](https://gitlab.com/Coll_J/praktikum-2-sda/blob/master/praktikum-2-sda/ahha2.c)

1. Scan n dan k, hitung jumalah hari maksimal (nk) `nk = n-k+1;`
2. scan angka sebanyak n sambil pre-compute biar ga tle, setiap kali input langsung diproses dengan cara:

* buat 2 deque, yang pertama untuk tempat sementara(q1), yang kedua untuk deque fix berisi angka yang diambil setiap hari nya
* buat variabel counting = 1 (c), bertambah setiap kali input
* setiap angka yang diambil push ke q1, tetapi jika q1 tidak kosong dan top nya lebih kecil dari angka yang diinput, pop q1 sebelum push dengan angka yang diinput, berikut implementasinya:
```c
while(q1->head!=NULL && q1->head->data<a)
{
    popf(q1);
}
pushf(q1, a, 0);
```
* jika c sudah lebih dari k, dan ukuran q2 lebih kecil dari nk lakukan proses memasukkan ke q2
* jika ukuran q1 lebih kecil dari k, ambil angka paling belakang q1 tanpa pop, lalu push ke q2
* jika tidak, pop belakang q1, ambil angkanya, push ke q2

berikut code nya:
```c
if(c>=k && q2->size<nk)
{
    if(q1->size<k)
    {
        p = end(q1);
        pushf(q2, p, hr);
        hr++;
    }
    else
    {
        p = popb(q1);
        pushf(q2, p, hr);
        hr++;
    }
}
```
3. inisiasi q1 untuk menyimpan nilai maks, buat deque baru q3 untuk menyimpan stok, hari = 0
4. scan hari(h) dan query(q), looping selama nilainya tidak -1, jika q1 tidak kosong maks = top q1, jika kosong maks = 0
* loop sebanyak h-hari, hari menjadi h
* pop belakang q2, simpan nilainya(p), push ke q3. jika p lebih besar dari maks sekarang, maks = p, push p ke q1

berikut code nya:
```c
if(q1->head!=NULL || q1->size!=0) maks = top(q1);
else maks = 0;
nk = h-hr;
hr = h;
while(nk--)
{
    p = popb(q2);
    pushf(q3, p, 0);
    if(p>=maks) 
    {
        pushf(q1, p, 0);
        maks = p;
    }
}
```
5. scan command sebanyak q
6. jika command 1:
* jika q3 kosong print "Stok abis"
* jika tidak kosong, pop depan q3 simpan pada p, jika p == top q1, pop depan q1
* print Nyam
7. jika command 2:
* jika q3 kosong print "Stok abis"
* jika tidak kosong, print top q1
```c
switch(cmd)
{
    case 1:
    if(q3==NULL || q3->size == 0) printf("Stok abis\n");
    else
    {
        p = popf(q3);
        if(p == top(q1)) popf(q1);
        printf("Nyam\n");
    }
    break;
    case 2:
    if(q3==NULL || q3->size == 0) printf("Stok abis\n");
    else
    {
        printf("%d\n", top(q1));
    }
    break;
}
```
##### balik ke [atas](#soal)

## EZTR
---

Soal
---
kita disuruh mengecek apakah setiap bracket ada pasangannya

contoh:
* (((()))) print :v
* ({} print v:

Jawab
---
### file code [disini](https://gitlab.com/Coll_J/praktikum-2-sda/blob/master/praktikum-2-sda/eztr.c)

1. inputan dalam string, setiap kurung buka **(, {, [** push ke dalam stack
2. setiap kurung tutup, cek apakah top di stack adalah pasangannya. jika iya, pop.
```c
if(input[i]=='(' || input[i]=='{' || input[i]=='[')
{
    push(&stacc, input[i]);
}
else if(input[i]==')' || input[i]=='}' || input[i]==']')
{
    if(input[i]==')' && top(stacc)=='(') pop(&stacc);
    else if(input[i]=='}' && top(stacc)=='{') pop(&stacc);
    else if(input[i]==']' && top(stacc)=='[') pop(&stacc);
}
```
3. cek jika stack kosong atau tidak. jika iya, print :v, jika tidak print v:.

##### balik ke [atas](#soal)

## SDM
Soal
---
disuruh nyari tau itu strukdat stack/queue/keduanya

Jawab
---
### file code [disini](https://gitlab.com/Coll_J/praktikum-2-sda/blob/master/praktikum-2-sda/sdm.c)

1. buat 4 flag untuk stack, queue, error, tidak tau.
2. alokasi strukdat
3. kalo command 1 push
4. kalo command 2:
* stack kosong error = 1
* sama kyk head dan tail, stack = 1, queue = 1
* sama kyk head doang, stack = 1, queue = 0
* sama kyk tail doang, stack = 0, queue = 1
* sama kyk head/tail doang, tp queue/stack ga sama(1 atau 0), tidak tahu = 1
* ga sama kyk dua2 nya, tidak tahu = 1

##### balik ke [atas](#soal)

## TDL
Soal
---
nyari jalan

Jawab
---
### file code [disini](https://gitlab.com/Coll_J/praktikum-2-sda/blob/master/praktikum-2-sda/tdl.c)

pake rekursi aja, tp dibikin strukdat. rekursi nya kyk rat in a maze. pake array of stack.
tapi kalo dipikir2 bisa ga sih mas kalo bikin strukdat tapi linker nya 4 arah tp ga stack dong ekwkwkw. maap.

##### balik ke [atas](#soal)
